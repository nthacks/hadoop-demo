### Pull the docker image 
`docker pull nthacks/hadoop-demo`

### Run the docker image
`docker run -it --rm nthacks/hadoop-demo`


### Disable safe mode (Read Only mode) so we can write to HDFS
`hdfs dfsadmin -safemode leave`


### Load the local data (words.txt) into HDFS
`hdfs dfs -put words.txt /words.txt`


### Run the MapReduce job

~~~
hadoop jar $HADOOP_LIB/hadoop-streaming-2.7.0.jar \
	-files mapper.py,reducer.py \
	-mapper mapper.py \
	-reducer reducer.py	\
	-input /words.txt \
	-output /wordcount
~~~

### Check if successful (You should see a file named _SUCCESS)
`hadoop fs -ls /wordcount`


### See the output on the terminal
`hadoop fs -cat /wordcount/part-00000`
